---
layout: default
---

## Byron - Cloud-Native Reactive Microservices Framework

## Informations
- **Members**: João Franscisco Lino Daniel e Leonardo Lana Violin Oliveira
- **Supervisor**: Prof. Dr. Alfredo Goldman e Renato Cordeiro

## Summary
Building and deploying complex distributed systems is hard. There are a lot of aspects that increase the chances of failure and there are a handful of tasks that make the development process repetitive and time consuming. Automation, observability, availability and continuous delivery are some of the key concerns in order to succeed in this task.

The aim of this project is to build Byron, a framework that helps developers to work around the initial overload in their processes to build cloud-native reactive microservices applications, i.e., systems that use a state of the art architecture to achieve these goals.

Byron will define a Domain Specific Language (DSL) to represent the application's entities and their relationships, allowing developers to focus on the business domain. It will also count with a Command Line Interface (CLI) that will be the single tool to set up the app in the cloud by bootstrapping basic resources and rolling new releases.

This way, Byron will make it easier to create and deploy distributed systems for complex domains. It will do this by providing an easy developer interface with an intuitive developing workflow, which will increase development speed by automating the repetition, and by default supporting a set of best technical decisions.

### Proposal
[Online](<< link para o arquivo >>) | [Download](<< link para o raw >>)

### Contact
Send a message to leolanavo at gmail.com or << email do joão >>